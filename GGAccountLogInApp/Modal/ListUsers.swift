//
//  ListUsers.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/21/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class ListUsers {
    var fullName : String
    var email: String

    init(fullName: String, email: String) {
        self.fullName = fullName
        self.email = email
    }
}
