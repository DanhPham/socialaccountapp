//
//  User.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/20/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class User {
    var userId : String?
    var idToken : String?
    var fullName : String?
    var givenName : String?
    var familyName : String?
    var email : String?
    
    init(userId: String, idToken: String, fullName: String, givenName: String, familyName: String, email: String) {
        self.userId = userId
        self.idToken = idToken
        self.givenName = givenName
        self.familyName = familyName
        self.fullName = fullName
        self.email = email
    }
}

