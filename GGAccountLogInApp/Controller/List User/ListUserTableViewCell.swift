//
//  ListUserTableViewCell.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/21/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ListUserTableViewCell : UITableViewCell {
    
    //MARK: Outlet
    @IBOutlet weak var lbFullname: UILabel!
    @IBOutlet weak var lbEmail: UILabel!
    
}

