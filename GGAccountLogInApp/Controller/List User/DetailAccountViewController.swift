//
//  DetailAccountViewController.swift
//  GGAccountLogInApp
//
//  Created by apple on 10/2/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import CoreData

class DetailAccountViewController : UIViewController {
    
    //MARK: Properties
    var selectedUser: NSManagedObject!
    
    //MARK: Outlet
    @IBOutlet weak var tvfUserID: UITextView!
    @IBOutlet weak var tvfGivenName: UITextView!
    @IBOutlet weak var tvfFamilyName: UITextView!
    @IBOutlet weak var tvfFullName: UITextView!
    @IBOutlet weak var tvfEmail: UITextView!
    
    @IBAction func btnDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvfUserID.text = selectedUser.value(forKey: "userId") as! String
        tvfGivenName.text = selectedUser.value(forKey: "givenName") as! String
        tvfFamilyName.text = selectedUser.value(forKey: "familyName") as! String
        tvfFullName.text = selectedUser.value(forKey: "fullName") as! String
        tvfEmail.text = selectedUser.value(forKey: "email") as! String
    }
}
