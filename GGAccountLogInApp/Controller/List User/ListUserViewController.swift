//
//  ListUserViewController.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/20/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import CoreData

class ListUserViewController : UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    //MARK: Properties
    
    
    var coreDataListUser: [NSManagedObject] = []
    var selectedUser: NSManagedObject?
    //MARK: Life Cycle
    override func viewDidLoad() {
        listUser.delegate = self
        listUser.dataSource = self

        save(fullname: "Danh Pham", email: "kennydvirgo@gmail.com", givenName: "Danh", familyName: "Pham", userID: "0101")
        save(fullname: "Dat Van To", email: "crazyman@gmail.com", givenName: "Dat", familyName: "To", userID: "0102")
        fetch()
        
        self.navigationController?.navigationBar.isHidden = true
    }
    //MARK: Outlet
    @IBOutlet weak var listUser: UITableView!

    
    //MARK: Action
    @IBAction func btnHamburgerClicked(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    //MARK: Function
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coreDataListUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = listUser.dequeueReusableCell(withIdentifier: "accountcell", for: indexPath) as! ListUserTableViewCell

        let user = coreDataListUser[indexPath.row]
//        if let username = user.value(forKey: "fullName") as? String {
//
        
//
//        }
          cell.lbFullname.text = user.value(forKey: "fullName") as? String
          cell.lbEmail.text = user.value(forKey: "email") as? String
        
//        cell.lbFullname.text = "124"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedUser = coreDataListUser[indexPath.row]
        self.performSegue(withIdentifier: "segueDetail", sender: self)
    }
    
    func save(fullname: String,email: String,givenName: String,familyName: String,userID: String){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Users", in: managedContext)!
        let user = NSManagedObject(entity: entity, insertInto: managedContext)
        user.setValue(fullname, forKey: "fullName")
        user.setValue(email, forKey: "email")
        user.setValue(userID, forKey: "userId")
        user.setValue(familyName, forKey: "familyName")
        user.setValue(givenName, forKey: "givenName")
        
        do {
            try managedContext.save()
            coreDataListUser.append(user)
            self.listUser.reloadData()
        } catch let error {
            print(error)
        }
    }
    
    func fetch(){
        
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }

        let managedContext =
            appDelegate.persistentContainer.viewContext
        

        //2
        let fetchUserRequest = NSFetchRequest<NSManagedObject>(entityName: "Users")
        //3
        do {
            
            coreDataListUser = try managedContext.fetch(fetchUserRequest)
            for i in coreDataListUser {
//                print(i.value(forKey: "email") ?? "")
//                print(i.value(forKey: "fullName") ?? "")
                
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        
        listUser.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetail" {
            let destination = segue.destination as! DetailAccountViewController
            destination.selectedUser = self.selectedUser
        }
    }
    
}

