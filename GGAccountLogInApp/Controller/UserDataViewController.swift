//
//  UserDataViewController.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/20/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GoogleSignIn
import SlideMenuControllerSwift

class UserDataViewController : UIViewController {
    
    //MARK: Properties
    var user1: User!
    
    //MARK: Outlet
    @IBOutlet weak var tvUserid: UITextView!
    @IBOutlet weak var tvGivenName: UITextView!
    @IBOutlet weak var tvFamilyName: UITextView!
    @IBOutlet weak var tvFullName: UITextView!
    @IBOutlet weak var email: UITextView!
    
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        tvUserid?.text = appDelegate.myUser?.userId
        tvGivenName?.text = appDelegate.myUser?.givenName
        tvFamilyName?.text = appDelegate.myUser?.familyName
        tvFullName?.text = appDelegate.myUser?.fullName
        email?.text =  appDelegate.myUser?.email
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    //MARK: Function
    
    func showAlert(title: String,message: String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let logInViewController = storyboard.instantiateViewController(withIdentifier: "GGLogInViewController")
            self.present(logInViewController, animated: true, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Action
    
    @IBAction func btnHamburgerClicked(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func btnLogOutClicked(_ sender: Any) {
        
        GIDSignIn.sharedInstance()?.signOut()
        
        showAlert(title: "Thông báo", message: "Bạn chắn chắn muốn đăng xuất?")
        
        
    }
    
}

