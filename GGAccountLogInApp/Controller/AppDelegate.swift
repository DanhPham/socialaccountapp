//
//  AppDelegate.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/20/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GoogleSignIn
import CoreData
import GoogleMaps
import GooglePlaces
import SlideMenuControllerSwift

let appDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var myUser: User?
    var locationManager = CLLocationManager()
    
    public func createMenuView() {
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "HomeScreenController") as! HomeScreenController
        mainViewController.userdetail = self.myUser
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        leftViewController.user2 = self.myUser
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let userViewController = storyboard.instantiateViewController(withIdentifier: "UserDataViewController") as! UserDataViewController
        let listViewController = storyboard.instantiateViewController(withIdentifier: "ListUserViewController") as! ListUserViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
//        let nvcLeftView: UINavigationController = UINavigationController(rootViewController: leftViewController)
        
//        UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
//        leftViewController.mainViewController = nvc
        
        let slideMenuController = SlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GIDSignIn.sharedInstance()?.clientID = "675937293332-ohnbbkq4ijelgo4kqjicddkqkrc0b9f6.apps.googleusercontent.com"
        
        GMSServices.provideAPIKey("AIzaSyCicutpDghhHrhR1mBtv94Dob00O4NhRyY")
        GMSPlacesClient.provideAPIKey("AIzaSyCicutpDghhHrhR1mBtv94Dob00O4NhRyY")
        
        locationManager.requestAlwaysAuthorization()
//        let checkLogin = UserDefaults.standard.bool(forKey: "CHECK_LOGIN")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let logInViewController = storyboard.instantiateViewController(withIdentifier: "GGLogInViewController")
        self.window?.rootViewController = logInViewController
        self.window?.makeKeyAndVisible()
        return true
    }
//    fileprivate func createMenuView() {
//
//        // create viewController code...
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! HomeScreenController
//        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
//        let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! LeftViewController
//
//        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
//
//        UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
//
//        leftViewController.mainViewController = nvc
//
//        let slideMenuController = SlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
//        slideMenuController.automaticallyAdjustsScrollViewInsets = true
//        slideMenuController.delegate = mainViewController as! SlideMenuControllerDelegate
//        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
//        self.window?.rootViewController = slideMenuController
//        self.window?.makeKeyAndVisible()
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Coredatabase")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    

}

