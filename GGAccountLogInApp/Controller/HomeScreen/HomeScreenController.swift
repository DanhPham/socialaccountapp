//
//  HomeScreenController.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/25/18.
//  Copyright © 2018 Apple. All rights reserved.
//
import UIKit
import GoogleMaps
import GooglePlaces
import SlideMenuControllerSwift

class HomeScreenController : UIViewController,CLLocationManagerDelegate{
    
    //MARK: Properties
    var userdetail : User!
    var hamburgerMenuIsVisible = false
    var locationManager = CLLocationManager()
    var currentLocation : CLLocation?
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    
    
    // An array to hold the list of likely places.
    var likelyPlaces: [GMSPlace] = []
    
    // The currently selected place.
    var selectedPlace: GMSPlace?
    var googleMapView: GMSMapView!
    //MARK: Outlet
    
    let marker = GMSMarker()
    let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
    
//    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        
        self.googleMapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        self.mainView.addSubview(googleMapView)
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = googleMapView
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        placesClient = GMSPlacesClient.shared()
        
//        googleMapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
//        googleMapView.settings.myLocationButton = true
        googleMapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        googleMapView.isMyLocationEnabled = true
//        self.addDoneButtonOnKeyboard()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            view.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
    // Populate the array with the list of likely places.


    //MARK: Action
    @IBAction func btnHamburgerClicked(_ sender: Any) {
        
        self.slideMenuController()?.openLeft()
        
        
    }
    @IBAction func btnSearchClicked(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueSlideMenu"{
            let destination = segue.destination as! LeftViewController
            let user = appDelegate.myUser
            destination.user2 = userdetail
        }
        
    }
    

}

extension HomeScreenController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
    }
    
    func leftDidOpen() {
    }
    
    func leftWillClose() {
    }
    
    func leftDidClose() {
    }
    
    func rightWillOpen() {
    }
    
    func rightDidOpen() {
    }
    
    func rightWillClose() {
    }
    
    func rightDidClose() {
    }
}
extension HomeScreenController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        print("Place ID: \(place.placeID)")
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        
        let camera1 = GMSCameraPosition.camera(withTarget: place.coordinate, zoom: zoomLevel)
        googleMapView.camera = camera1
        googleMapView.animate(to: camera1)
        


//                // 2. Perform UI Operations.
//                
        self.marker.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        self.marker.title = "\(place.formattedAddress)"
        self.marker.map = self.googleMapView

        dismiss(animated: true,completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
