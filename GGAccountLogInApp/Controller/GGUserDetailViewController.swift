//
//  GGUserDetailViewController.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/20/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class GGUserDetailViewController : UIViewController {
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        
    }
    
    //MARK: Properties
    var userdetail : User!
    
    //MARK: Action
    @IBAction func btnUserDataClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "segueDetailUserData", sender: self)
    }
    
    @IBAction func btnListUserClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "segueDetailListUser", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetailUserData"{
            let destination = segue.destination as! UserDataViewController
            destination.user1 = userdetail
        }
        if segue.identifier == "segueDetailListUser" {
            let destination = segue.destination as! ListUserViewController
        }
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
