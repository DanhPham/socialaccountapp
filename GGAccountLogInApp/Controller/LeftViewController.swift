//
//  LeftViewController.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/25/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LeftViewController : UIViewController{

    //MARK: Properties
    var user2 : User!
    var homeViewController : UIViewController!
    var userViewController : UIViewController!
    var listViewController : UIViewController!
    
    //MARK: Properties
    @IBOutlet weak var imageAvatar: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageAvatar.layer.cornerRadius = 50
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeScreenController") as! HomeScreenController
        self.homeViewController = UINavigationController(rootViewController: homeViewController)
        
        let userViewController = storyboard.instantiateViewController(withIdentifier: "UserDataViewController") as! UserDataViewController
        self.userViewController = UINavigationController(rootViewController: userViewController)
        let listViewController = storyboard.instantiateViewController(withIdentifier: "ListUserViewController") as! ListUserViewController
        self.listViewController = UINavigationController(rootViewController: listViewController)
        
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    @IBAction func btnUserDataClicked(_ sender: Any) {
        self.slideMenuController()?.changeMainViewController(userViewController, close: true)
    }

    @IBAction func btnListUserClicked(_ sender: Any) {
        self.slideMenuController()?.changeMainViewController(listViewController, close: true)
    }
    
    @IBAction func btnMapClicked(_ sender: Any) {
        self.slideMenuController()?.changeMainViewController(homeViewController, close: true)
    }
    
    
}
