//
//  ContainerViewController.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/25/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class ContainerViewController : SlideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    override func awakeFromNib() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "Main") {
            self.mainViewController = controller
        }
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "Left") {
            self.leftViewController = controller
        }
        super.awakeFromNib()
    }
}
