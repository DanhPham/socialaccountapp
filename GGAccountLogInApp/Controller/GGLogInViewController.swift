//
//  ViewController.swift
//  GGAccountLogInApp
//
//  Created by apple on 9/20/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GoogleSignIn

class GGLogInViewController: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate {
    
    //MARK: Properties
    var userdata : User!
    var logInStatus = false
    
    //MARK: Outlet
    @IBOutlet weak var btnLogIn: UIButton!
    
    //MARK:Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        btnLogIn.layer.cornerRadius = 30
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.uiDelegate = self
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!){
        if let error = error {
            logInStatus = false
//            UserDefaults.standard.set(false, forKey: "CHECK_LOGIN")
            print("\(error.localizedDescription)")
            // [START_EXCLUDE silent]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
            // [END_EXCLUDE]
        } else {
            // Perform any operations on signed in user here.
            logInStatus = true
//            UserDefaults.standard.set(true, forKey: "CHECK_LOGIN")
            userdata = User(userId: user.userID, idToken: user.authentication.idToken, fullName: user.profile.name, givenName: user.profile.givenName, familyName: user.profile.familyName, email: user.profile.email)
            // [START_EXCLUDE]
            print(userdata)
            appDelegate.myUser = userdata
            appDelegate.createMenuView()
        }
    }
    
    func signinButtonClicked (completedHandler: @escaping(Bool) -> ()) {
        GIDSignIn.sharedInstance()?.signIn()
        completedHandler(true)
        logInStatus = true
    }
    
    //MARK: Action
    @IBAction func btnGGLoginClicked(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signIn()
        logInStatus = false
//        btnLogIn.isEnabled = false
    }
    
    //MARK: Function
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueHomeScreen" {
            let destination = segue.destination as! HomeScreenController
            destination.userdetail = userdata
        }
    }

}

